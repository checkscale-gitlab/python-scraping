from selenium import webdriver
from time import sleep

SURVEY_URL = "https://www.survio.com/survey/d/N1M8B4I8V6N7X8E8O"

browser = webdriver.Firefox()
browser.maximize_window()

# Scraping process
browser.get(SURVEY_URL)

start_survey_button = browser.find_element_by_class_name("start")
# Clicking workaround
sleep(1)
browser.save_screenshot("./screenshots/screen_1.png")
browser.execute_script("arguments[0].click();", start_survey_button)
sleep(1)
browser.save_screenshot("./screenshots/screen_2.png")

# Beginning the survey filling out
try:
    survey = browser.find_element_by_css_selector(".align > div:nth-child(2)")
    questions = survey.find_elements_by_class_name("block")
    for index, question in enumerate(questions, start=1):
        if index > 6:
            text_input = question.find_element_by_id("text57823449")
            text_input.send_keys("My answer")
        else:
            answer = question.find_element_by_class_name("item.flaticon-frontend-check")
            browser.execute_script("arguments[0].click();", answer)
except Exception as e:
    print(f'Error: {e}')

# Sending answers
send_button = browser.find_element_by_css_selector(
    ".desktop-footer > div:nth-child(2) > div:nth-child(2) > div:nth-child(1)"
)
send_button.click()
sleep(2)
browser.save_screenshot("./screenshots/screen_3.png")

browser.close()
